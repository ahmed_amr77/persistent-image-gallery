//
//  ImageGalleryCollectionViewCell.swift
//  ImageGallery
//
//  Created by Ahmed Sharf on 1/14/22.
//

import UIKit

class ImageGalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak private var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak private var imageView: UIImageView!
    
    var image: UIImage? {
        didSet {
            imageView.image = image
            spinner.stopAnimating()
            if image != nil {
                isUserInteractionEnabled = true
            } else {
                isHidden = true
            }
        }
    }
}
