//
//  ImageGalleryCollectionViewController.swift
//  ImageGallery
//
//  Created by Ahmed Sharf on 1/14/22.
//

import UIKit

class ImageGalleryCollectionViewController: UICollectionViewController {
    
    var gallery = Gallery(title: "Untitled")
    
    var flowLayout: UICollectionViewFlowLayout? {
        return collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
    }
    private var minimumItemWidth: CGFloat {
        return (collectionView.frame.size.width / 3) - (itemSpacing * 2)
    }
    private var maximumItemWidth: CGFloat {
        return collectionView.frame.size.width
    }
    let itemSpacing: CGFloat = 8
    var itemWidth: CGFloat = 0 {
        didSet {
            if itemWidth > maximumItemWidth {
                itemWidth = maximumItemWidth
            }
            else if itemWidth < minimumItemWidth {
                itemWidth = minimumItemWidth
            }
        }
    }
    
    var document: ImageGalleryDocument?
    
    //                                           50 MB                   250 MB
    private var cache: URLCache?
    private var session = URLSession(configuration: .default)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        document?.open(completionHandler: { (success) in
            if success {
                self.title = self.document?.localizedName
                self.gallery = self.document?.gallery ?? self.gallery
                self.collectionView.reloadData()
            }
        })
    }

    override func viewWillLayoutSubviews() {
        // to fix disappearing of back button in portrait mode
        if UIDevice.current.orientation.isLandscape {
            navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
            navigationItem.leftItemsSupplementBackButton = false
        } else {
            navigationItem.leftBarButtonItem = nil
            navigationItem.leftItemsSupplementBackButton = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //                                         50 MB                   250 MB
        URLCache.shared = URLCache(memoryCapacity: 52428800, diskCapacity: 262144000)
        cache = URLCache.shared
        
        itemWidth = (collectionView.frame.size.width / 2) - itemSpacing

        collectionView.dropDelegate = self
        collectionView.dragDelegate = self
        collectionView.dragInteractionEnabled = true
        
        collectionView.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(self.resizeSelectedLabel(by:))))
    }
    
    @objc func resizeSelectedLabel(by recognizer: UIPinchGestureRecognizer) {
        switch recognizer.state {
        case .began, .changed:
            itemWidth *= recognizer.scale
            flowLayout?.invalidateLayout()
            recognizer.scale = 1.0
        default:
            break
        }
    }
    
    @IBAction func close(_ sender: UIBarButtonItem) {
        if document?.gallery != nil {
            document?.thumbnail = nil
            for image in gallery.images {
                if let url = image.url {
                    let request = URLRequest(url: url)
                    if let data = self.cache?.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
                        document?.thumbnail = image
                        break
                    }
                }
            }
        }
        documentChanged()
        dismiss(animated: true) {
            self.document?.close()
        }
    }
    
    func documentChanged() {
        document?.gallery = gallery
        if document?.gallery != nil {
            document?.updateChangeCount(.done)
        }
    }
    
    // MARK:- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowImage",
            let imageVC = segue.destination as? ImageViewController,
            let imageCell = sender as? UICollectionViewCell {
            
            if let indexPath = collectionView.indexPath(for: imageCell) {
                imageVC.url = gallery.images[indexPath.item].url
            }
        }
    }

    // MARK:- UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gallery.images.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageGalleryCollectionViewCell", for: indexPath)
        if let imageCell = cell as? ImageGalleryCollectionViewCell {
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                guard let self = self else { return }
                guard let url = self.gallery.images[indexPath.item].url else { return }
                let request = URLRequest(url: url)
                if let data = self.cache?.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        imageCell.image = image
                    }
                } else {
                    guard let cache = self.cache else { return }
                    request.cacheImage(with: self.session, in: cache) { (resImage) in
                        DispatchQueue.main.async {
                            if let image = resImage {
                                imageCell.image = image
                            } else {
                                imageCell.image = nil
                            }
                        }
                    }
                }
            }
        }
        return cell
    }
}

// MARK:- FlowLayout
extension ImageGalleryCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemHeight = itemWidth / gallery.images[indexPath.item].ratio
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 21
    }
}

// MARK:- Drop
extension ImageGalleryCollectionViewController: UICollectionViewDropDelegate {
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        let isSelf = (session.localDragSession?.localContext) as? UICollectionView == collectionView
        return isSelf ? session.canLoadObjects(ofClass: UIImage.self) : session.canLoadObjects(ofClass: NSURL.self) && session.canLoadObjects(ofClass: UIImage.self)
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        let isSelf = (session.localDragSession?.localContext) as? UICollectionView == collectionView
        return UICollectionViewDropProposal(operation: isSelf ? .move : .copy, intent: .insertAtDestinationIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: gallery.images.count, section: 0)
        for item in coordinator.items {
            if let sourceIndexPath = item.sourceIndexPath {
                collectionView.performBatchUpdates {
                    gallery.move(from: sourceIndexPath.item, to: destinationIndexPath.item)
                    collectionView.deleteItems(at: [sourceIndexPath])
                    collectionView.insertItems(at: [destinationIndexPath])
                }
                coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
                documentChanged()
            } else {
                let placeholderContext = coordinator.drop(item.dragItem, to: UICollectionViewDropPlaceholder(insertionIndexPath: destinationIndexPath, reuseIdentifier: "PlaceholderCell"))
                var url: URL?
                item.dragItem.itemProvider.loadObject(ofClass: NSURL.self) { (urlProvider, _) in
                    if let draggedURL = urlProvider as? NSURL {
                        url = draggedURL as URL
                    } else {
                        DispatchQueue.main.async {
                            placeholderContext.deletePlaceholder()
                        }
                    }
                }
                item.dragItem.itemProvider.loadObject(ofClass: UIImage.self) { [weak self] (imageProvider, _) in
                    DispatchQueue.main.async {
                        if let image = imageProvider as? UIImage {
                            placeholderContext.commitInsertion { (insertionIndexPath) in
                                self?.gallery.add(image, with: url, at: insertionIndexPath)
                                self?.documentChanged()
                            }
                        }  else {
                            placeholderContext.deletePlaceholder()
                        }
                    }
                }
            }
        }
    }
}

// MARK:- Drag
extension ImageGalleryCollectionViewController: UICollectionViewDragDelegate {
    private func dragItem(at indexPath: IndexPath) -> [UIDragItem] {
        if let image = (collectionView.cellForItem(at: indexPath) as? ImageGalleryCollectionViewCell)?.image {
            let dragItem = UIDragItem(itemProvider: NSItemProvider(object: image))
            dragItem.localObject = image
            return [dragItem]
        } else {
            return []
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        session.localContext = collectionView
        return dragItem(at: indexPath)
    }
    
    //drag another items while dragging
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        return dragItem(at: indexPath)
    }
}
