//
//  Gallery.swift
//  ImageGallery
//
//  Created by Ahmed Sharf on 1/14/22.
//

import UIKit

struct Gallery: Codable {
    private(set) var images = [Image]()
    var title: String
    
    init(title: String) {
        self.title = title
    }
    
    // MARK:- JSON
    init?(json: Data) {
        if let newValue = try? JSONDecoder().decode(Gallery.self, from: json) {
            self = newValue
        } else {
            return nil
        }
    }
    
    var json: Data? {
        return try? JSONEncoder().encode(self)
    }
    
    // MARK:- Operations
    mutating func add(_ image: UIImage, with url: URL?, at index: IndexPath) {
        let ratio = image.size.width / image.size.height
        images.insert(Image(url: url, ratio: ratio), at: index.item)
    }
    
    mutating func move(from sourceIndex: Int, to destIndex: Int) {
        let movedItem = images.remove(at: sourceIndex)
        if destIndex < (images.count)  {
            images.insert(movedItem, at: destIndex)
        } else {
            images.append(movedItem)
        }
    }
    
    mutating func delete(at index: Int) {
        images.remove(at: index)
    }
}
