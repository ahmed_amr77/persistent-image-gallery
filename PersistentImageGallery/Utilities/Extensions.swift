//
//  Extensions.swift
//  ImageGallery
//
//  Created by Ahmed Sharf on 1/15/22.
//

import UIKit

extension URL {
    func fetchImage() -> UIImage? {
        var image: UIImage?
        if let urlContents = try? Data(contentsOf: self),
           let fetchedImage = UIImage(data: urlContents) {
            image = fetchedImage
        } else {
            image = UIImage(named: "placeholder")
        }
        return image
    }
}

extension URLRequest {
    func cacheImage(with session: URLSession, in cache: URLCache, completion: @escaping (UIImage?)->()) {
        var resultImage: UIImage?
        let task = session.dataTask(with: self) { (urlData, urlResponse, urlError) in
            if urlError != nil {
                print("Data request failed with error \(urlError!)")
                completion(nil)
            }
            if let data = urlData, let image = UIImage(data: data) {
                if let response = urlResponse {
                    cache.storeCachedResponse(CachedURLResponse(response: response, data: data), for: self)
                }
                resultImage = image
            } else {
                resultImage = UIImage(named: "placeholder")
            }
            completion(resultImage)
        }
        task.resume()
    }
}

extension String {
    func madeUnique(withRespectTo otherStrings: [String]) -> String {
        var possiblyUnique = self
        var uniqueNumber = 1
        while otherStrings.contains(possiblyUnique) {
            possiblyUnique = self + " \(uniqueNumber)"
            uniqueNumber += 1
        }
        return possiblyUnique
    }
}
